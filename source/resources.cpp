#include "IwGx.h"
#include "IwTween.h"
#include "bricks.h"
#include "game.h"
#include "main.h"
#include "resources.h"

Resources::Resources()
{
    Font = Iw2DCreateFont("fonts/arial8.gxfont");
}

Resources::~Resources()
{
    delete Font;
}
