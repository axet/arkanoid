#include "IwGx.h"
#include "IwTween.h"
#include "bricks.h"
#include "game.h"
#include "main.h"
#include "scenes.h"

using namespace IwTween;

Scene::Scene()
{
    m_tweener = new CTweenManager();
}

Scene::~Scene()
{
    delete m_tweener;
}

void Scene::Update(float deltaTime, float alphaMul)
{
    CNode::Update(deltaTime, alphaMul);
}

void Scene::Render()
{
    CNode::Render();
}

SceneManager::SceneManager()
{
    
}

SceneManager::~SceneManager()
{
    
}

void SceneManager::Update(float deltaTime, float alphaMul)
{
    m_current->Update(deltaTime, alphaMul);
}

void SceneManager::Render()
{
    m_current->Render();
}
