#if !defined(__BALL_H__)
#define __BALL_H__

#include "Iw2DSceneGraph.h"
#include <list>

#include "Box2D.h"

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

class Ball : public CDrawable
{
public:
    b2Body* m_body;

    Ball();
    ~Ball();

    void init(float x, float y);
    
    void Render();
    
    void Destory(b2World* w);
    void Update(b2World* w);
    
    float GetSize();
};

#endif
