#include "IwGx.h"
#include "IwTween.h"
#include "bricks.h"
#include "game.h"
#include "main.h"

using namespace IwTween;

const int num_columns = 8;
const int num_rows = 8;

Brick::Brick():m_body(0), m_delete(false)
{
}

Brick::~Brick()
{
    delete m_material;
    delete m_type;
}

void Brick::Render()
{
    CDrawable::PreRender();
    
    //Iw2DFillRect(CIwFVec2(0, 0), CIwFVec2(m_W, m_H));
    
    CIwFVec2 t = m_MatGlobal.TransformVec(CIwFVec2::g_Zero);
    CIwFVec2 tt = m_MatGlobal.RotateVec(CIwFVec2(m_W, m_H));

    IwGxSetMaterial(m_material);
    CIwSVec2 xx(t.x, t.y);
    CIwSVec2 ww(tt.x, tt.y);
    for(int i = 0; i < 4; i++)
    {
        m_type[i].a = (uint8)(m_Alpha * 0xff);
    }
    IwGxDrawRectScreenSpace(&xx, &ww, m_type);

    // draw border
    Iw2DSetColour(0xff232323);
    Iw2DDrawRect(CIwFVec2::g_Zero, CIwFVec2(m_W, m_H));
    
    CDrawable::Render();
}

void Brick::init(float x, float y, int type)
{
    m_W = BRICK_W;
    m_H = BRICK_H;

    m_X = x * m_W;
    m_Y = y * m_H;

    m_material = new CIwMaterial;
    m_material->SetColAmbient(0xFF, 0x00, 0x00, 0xFF);
    m_material->SetCullMode(CIwMaterial::CULL_NONE);
    m_material->SetAlphaMode(CIwMaterial::ALPHA_BLEND);
    
    m_type = new CIwColour[4];
    switch(type)
    {
        case 0:
            score = 50;
            m_Color = CColor(0xff,0,0,0xff);
            m_type[0].Set(0xFF, 0x00, 0x00, 0xff);
            m_type[1].Set(0x00, 0xFF, 0x00, 0xff);
            m_type[2].Set(0x00, 0x00, 0xFF, 0xff);
            m_type[3].Set(0xFF, 0x00, 0xFF, 0xff);
            break;
        case 1:
            score = 5;
            m_Color = CColor(0x00,0xff,0,0xff);
            m_type[0].Set(0x00, 0xFF, 0x00, 0xff);
            m_type[1].Set(0x00, 0xFF, 0x00, 0xff);
            m_type[2].Set(0x00, 0xFF, 0x00, 0xff);
            m_type[3].Set(0x00, 0xFF, 0x00, 0xff);
            break;
            
    }
}

void Brick::Update(b2World *w)
{
    if(m_body != 0){
        w->DestroyBody(m_body);
    }

    CIwFVec2 t = m_MatGlobal.TransformVec(CIwFVec2::g_Zero);

    float x = toGraphics(t.x);
    float y = toGraphics(t.y);

    b2BodyDef def;
    def.type = b2_staticBody;
    def.position.Set(toPhysics(x + m_W/2), toPhysics(y + m_H/2));
    m_body = w->CreateBody(&def);
    b2PolygonShape box;
    box.SetAsBox(toPhysics(m_W/2), toPhysics(m_H/2));
    b2FixtureDef fd;
    fd.shape = &box;
    fd.density = 0;
    fd.friction = 0;
    fd.restitution = 0;
    m_body->CreateFixture(&fd);
    m_body->SetUserData(this);
}

Bricks::Bricks()
{
    m_bricks = new Brick* [num_columns * (num_rows + 1)];

    m_Y = 60;
    m_X = 0;

    m_W = num_columns * BRICK_W;
    m_H = num_rows  * BRICK_H;
    
    NewGame();
}

void Bricks::NewGame()
{
    int index = 0;
    for (int y = 0; y < num_rows; y++)
    {
        int type = y % 2;
        for (int x = 0; x < num_columns; x++)
        {
            Brick *b = m_bricks[index];
            m_bricks[index] = new Brick();
            m_bricks[index]->init((float)x, (float)y, type);
            AddChild(m_bricks[index]);
            index++;
        }
    }
}

Bricks::~Bricks()
{
    int index = 0;
    for (int y = 0; y < num_rows; y++)
    {
        for (int x = 0; x < num_columns; x++)
        {
            Brick *b = m_bricks[index];
            if(b)
            {
                RemoveChild(b);
                delete b;
            }
            index++;
        }
    }

    delete []m_bricks;
}

void Bricks::Update(b2World *w)
{
    int index = 0;
    for (int y = 0; y < num_rows; y++)
    {
        for (int x = 0; x < num_columns; x++)
        {
            Brick *b = m_bricks[index];

            if(b) {
                if(b->m_delete) {
                    w->DestroyBody(b->m_body);
                    RemoveChild(b);
                    m_bricks[index] = 0;
                    delete b;
                }else {
                    b->Update(w);
                }
            }
            
            index++;
        }
    }
}

int Bricks::Hit(b2Body *v)
{
    int score = 0;
    
    void * vb = v->GetUserData();

    int index = 0;
    for (int y = 0; y < num_rows; y++)
    {
        for (int x = 0; x < num_columns; x++)
        {
            Brick *b = m_bricks[index];
            if(b == vb)
            {
                if(b->m_delete == false)
                {
                    b->m_delete = true;
                    score += b->score;
                }
            }
            index++;
        }
    }

    return score;
}
