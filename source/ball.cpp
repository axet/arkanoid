#include "IwGx.h"
#include "IwTween.h"
#include "bricks.h"
#include "game.h"
#include "main.h"

using namespace IwTween;

Ball::Ball():m_body(0)
{
    m_W = BRICK_H;
    m_H = BRICK_H;
    
    m_Color = CColor(0xff, 0xff, 0xff, 0xff);
}

Ball::~Ball()
{
}

void Ball::Render()
{
    CDrawable::Render();
    
    CDrawable::PreRender();

    Iw2DFillArc(CIwFVec2::g_Zero, CIwFVec2(m_W / 2 * m_ScaleX, m_H / 2 * m_ScaleY), 0, 2 * PI);
}

void Ball::init(float x, float y)
{
    m_X = x;
    m_Y = y;
}

float Ball::GetSize()
{
    return m_W;
}

void Ball::Destory(b2World *w)
{
    if(m_body != 0) {
        w->DestroyBody(m_body);
        m_body = 0;
    }
}

void Ball::Update(b2World *w)
{
    if(m_body != 0) {
        b2Vec2 pos = m_body->GetPosition();
        m_X = fromPhysics(pos.x);
        m_Y = fromPhysics(pos.y);
        return;
    }

    CIwFVec2 t = m_MatGlobal.TransformVec(CIwFVec2::g_Zero);

    float x = toGraphics(t.x);
    float y = toGraphics(t.y);

    b2BodyDef def;
    def.type = b2_dynamicBody;
    def.linearDamping = 0.0f;
    def.angularDamping = 0.0f;
    def.position.Set(toPhysics(x), toPhysics(y));
    m_body = w->CreateBody(&def);
    b2CircleShape shape;
    shape.m_p.SetZero();
    shape.m_radius = toPhysics(GetSize() / 2);
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 0;
    fd.friction = 0;
    fd.restitution = 1;
    m_body->CreateFixture(&fd);
    m_body->SetBullet(true);
    m_body->SetUserData(this);
}
