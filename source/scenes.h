#if !defined(__SCENE_H__)
#define __SCENE_H__

#include "Iw2DSceneGraph.h"
#include <list>

#include "Box2D.h"

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

class Scene : public CNode
{
public:
    CTweenManager* m_tweener;
    
    Scene();
    virtual ~Scene();
    
    virtual void Update(float deltaTime = 0.0f, float alphaMul = 1.0f);
    void Render();
};

class SceneManager : public CNode
{
public:
    Scene *m_current;

    SceneManager();
    ~SceneManager();

    virtual void Update(float deltaTime = 0.0f, float alphaMul = 1.0f);
    void Render();
    
};

#endif
