#if !defined(_INPUT_H)
#define _INPUT_H

#include "s3ePointer.h"


#define MAX_TOUCHES     10

class Input
{
public:
    int             m_X, m_Y;          
    bool            m_Touched;         
    int             m_PX, m_PY;        
    bool            m_PrevTouched;

public:
    Input();

    void            Update();
    void            Reset();

    static void     TouchButtonCB(s3ePointerEvent* event);
    static void     TouchMotionCB(s3ePointerMotionEvent* event);
    static void     MultiTouchButtonCB(s3ePointerTouchEvent* event);
    static void     MultiTouchMotionCB(s3ePointerTouchMotionEvent* event);
};

extern Input* g_pInput;


#endif  // _INPUT_H
