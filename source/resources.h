#if !defined(__RESOURCES_H__)
#define __RESOURCES_H__

#include "Iw2DSceneGraph.h"
#include <list>

#include "Box2D.h"

class Resources
{
public:
    CIw2DFont*      Font;

    Resources();
    ~Resources();

};

#endif
