#include "IwGx.h"
#include "IwTween.h"
#include "bricks.h"
#include "game.h"
#include "menu.h"
#include "main.h"
#include "input.h"

using namespace IwTween;

Menu::Menu()
{
    float fontScale = (float)IwGxGetScreenWidth() / FONT_DESIGN_WIDTH;
    float actualFontHeight = FONT_HEIGHT * fontScale;
    
    m_play = new CLabel();
    m_play->m_X = 80 * fontScale;
    m_play->m_Y = 50;
    m_play->m_W = FONT_DESIGN_WIDTH;
    m_play->m_H = actualFontHeight;
    m_play->m_Text = "Play";
    m_play->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    m_play->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    m_play->m_Font = g_pResources->Font;
    m_play->m_ScaleX = 4 * fontScale;
    m_play->m_ScaleY = 4 * fontScale;
    AddChild(m_play);

    m_exit = new CLabel();
    m_exit->m_X = 80 * fontScale;
    m_exit->m_Y = 250;
    m_exit->m_W = FONT_DESIGN_WIDTH;
    m_exit->m_H = actualFontHeight;
    m_exit->m_Text = "Exit";
    m_exit->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    m_exit->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    m_exit->m_Font = g_pResources->Font;
    m_exit->m_ScaleX = 4 * fontScale;
    m_exit->m_ScaleY = 4 * fontScale;
    AddChild(m_exit);
}

Menu::~Menu()
{
    RemoveChild(m_play);
    delete m_play;
    RemoveChild(m_exit);
    delete m_exit;
}

void Menu::Render()
{
    Scene::Render();
}

void Menu::Update(float deltaTime, float alphaMul)
{
    Scene::Update(deltaTime, alphaMul);
    
    if (!g_pInput->m_Touched && g_pInput->m_PrevTouched)
    {
        g_pInput->Reset();
        
        if(m_play->HitTest(g_pInput->m_X, g_pInput->m_Y))
        {
            g_pSceneManager->m_current = g_pGame;
            g_pGame->NewGame();
        }

        if(m_exit->HitTest(g_pInput->m_X, g_pInput->m_Y))
        {
            s3eDeviceExit();
        }
    }
}
