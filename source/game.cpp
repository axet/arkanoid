#include "IwGx.h"
#include "IwHashString.h"

#include "game.h"
#include "main.h"
#include "input.h"
#include "resources.h"

Game::Game():m_bricks(0), m_scores(0), wp(0), m_board(0)
{
    m_gamestate = ready;
    
    m_X = 0;
    m_Y = 0;
    m_ScaleX = G_SCALE;
    m_ScaleY = G_SCALE;

    m_graphicsScale = IwGxGetScreenWidth() / (float)GRAPHICS_WIDTH;

    m_bricks = new Bricks();
    AddChild(m_bricks);

    m_board_y_area = IwGxGetScreenHeight() - IwGxGetScreenHeight() * 2 * (float)BOARD_POSITION / 100;
    m_board_y = IwGxGetScreenHeight() - IwGxGetScreenHeight() * (float)BOARD_POSITION / 100;

    m_ball = new Ball();
    AddChild(m_ball);

    // UI
    
    float fontScale = (float)IwGxGetScreenWidth() / FONT_DESIGN_WIDTH;
    float actualFontHeight = FONT_HEIGHT * fontScale;

    m_scoreLabel = new CLabel();
    m_scoreLabel->m_X = 0;
    m_scoreLabel->m_Y = 0;
    m_scoreLabel->m_W = FONT_DESIGN_WIDTH;
    m_scoreLabel->m_H = actualFontHeight;
    m_scoreLabel->m_Text = "Score: 0";
    m_scoreLabel->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    m_scoreLabel->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    m_scoreLabel->m_Font = g_pResources->Font;
    m_scoreLabel->m_ScaleX = fontScale;
    m_scoreLabel->m_ScaleY = fontScale;
    AddChild(m_scoreLabel);

    m_liveLabel = new CLabel();
    m_liveLabel->m_X = 80 * fontScale;
    m_liveLabel->m_Y = 0;
    m_liveLabel->m_W = FONT_DESIGN_WIDTH;
    m_liveLabel->m_H = actualFontHeight;
    m_liveLabel->m_Text = "Lives: 0";
    m_liveLabel->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    m_liveLabel->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    m_liveLabel->m_Font = g_pResources->Font;
    m_liveLabel->m_ScaleX = fontScale;
    m_liveLabel->m_ScaleY = fontScale;
    AddChild(m_liveLabel);

    NewGame();
}

void Game::NewGame()
{
    m_scores = 0;
    m_lives = 3;

    if(wp)
      delete wp;

    b2Vec2 gravity(0.0f, 0.0f);
    wp = new b2World(gravity);
    wp->SetContactListener(this);
    
    const int BORDER = 5;
    
    // top
    AddBorder(0, -BORDER, IwGxGetScreenWidth(), BORDER);
    // left
    AddBorder(-BORDER, 0, BORDER, IwGxGetScreenHeight());
    // right
    AddBorder(IwGxGetScreenWidth(), 0, BORDER, IwGxGetScreenHeight());
    // bottom
    ground = AddBorder(0, IwGxGetScreenHeight(), IwGxGetScreenWidth(), BORDER);

    if(m_ball)
    {
        RemoveChild(m_ball);
        delete m_ball;
    }
    m_ball = new Ball();
    AddChild(m_ball);

    if(m_board)
    {
        RemoveChild(m_board);
        delete m_board;
    }
    m_board = new Board();
    m_board->init(toGraphics(IwGxGetScreenWidth()) / 2 - m_board->GetWidth() / 2, toGraphics(m_board_y));
    AddChild(m_board);

    if(m_bricks)
    {
        RemoveChild(m_bricks);
        delete m_bricks;
    }
    m_bricks = new Bricks();
    AddChild(m_bricks);
}

b2Body* Game::AddBorder(int x, int y, int xx, int yy)
{
    x = toGraphics(x);
    y = toGraphics(y);
    xx = toGraphics(xx);
    yy = toGraphics(yy);
    
    b2BodyDef def;
    def.type = b2_staticBody;
    def.position.Set(toPhysics(x + xx/2), toPhysics(y + yy / 2));
    b2Body* r = wp->CreateBody(&def);

    b2PolygonShape box;
    box.SetAsBox(toPhysics(xx/2), toPhysics(yy/2));
    b2FixtureDef fd;
    fd.shape = &box;
    fd.density = 0;
    fd.friction = 0;
    fd.restitution = 0;
    r->CreateFixture(&fd);

    r->SetUserData(this);
    
    return r;
}

Game::~Game()
{
    if(m_bricks)
    {
        RemoveChild(m_bricks);
        delete m_bricks;
    }
    if(m_ball)
    {
        RemoveChild(m_ball);
        delete m_ball;
    }
    if(m_board)
    {
        RemoveChild(m_board);
        delete m_board;
    }
    delete wp;
    RemoveChild(m_scoreLabel);
    delete m_scoreLabel;
    RemoveChild(m_liveLabel);
    delete m_liveLabel;
}

void Game::Update(float deltaTime, float alphaMul)
{
    Scene::Update(deltaTime, alphaMul);
    
    if (g_pInput->m_Touched)
    {
        if(g_pInput->m_PY > m_board_y_area) {
            m_board->m_X = toGraphics(g_pInput->m_X) - m_board->GetWidth() / 2;

            if (m_board->m_X < 0)
                m_board->m_X = 0;

            int max = toGraphics(IwGxGetScreenWidth()) - m_board->GetWidth();
            if(m_board->m_X > max)
                m_board->m_X = max;
            
            m_board->Update(wp);
        }
    }else
    if (!g_pInput->m_Touched && g_pInput->m_PrevTouched)
    {
        g_pInput->Reset();
        
        if(m_gamestate == ready)
        {
            m_ball->Update(wp);

            b2Body *b = m_ball->m_body;
            b->ApplyLinearImpulse(b2Vec2(10, -10), b->GetPosition());
            m_gamestate = playing;
        }
    }
    
    if(m_gamestate == ready)
    {
        InitBall();
    }

    // The suggested iteration count for Box2D is 8 for velocity and 3 for position
    wp->Step(deltaTime, 8, 3);
    
    if (m_gamestate == lost) {
        g_pGame->m_lives--;
        if (g_pGame->m_lives <= 0) {
            g_pSceneManager->m_current = g_pMenu;
        }

        m_gamestate = restarting;
        if(m_ball != 0) {
            m_ball->Destory(wp);

            g_pTweener->Tween(1.0f,
                              FLOAT, &m_ball->m_Alpha, 0.0f,
                              ONCOMPLETE, DeleteBall,
                              END);
        }
    }

    if(m_gamestate == playing)
      m_ball->Update(wp);

    m_bricks->Update(wp);
    
    // update UI
    char str[64];
    snprintf(str, sizeof(str), "Score: %d", m_scores);
    m_scoreLabel->m_Text = str;

    snprintf(str, sizeof(str), "Lives: %d", m_lives);
    m_liveLabel->m_Text = str;
}

void Game::InitBall()
{
    m_ball->m_Alpha = 1;
    m_ball->Destory(wp);
    m_ball->m_Y = m_board->m_Y - m_ball->GetSize() / 2;
    m_ball->m_X = m_board->m_X + m_board->GetWidth() / 2;
}

void Game::Render()
{
    CNode::Render();
}

void Game::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
    b2Body *b1 = contact->GetFixtureA()->GetBody();
    b2Body *b2 = contact->GetFixtureB()->GetBody();

    // increace moving speed by 1% each hit
    b2Vec2 v = m_ball->m_body->GetLinearVelocity();
    v.x *= 0.01;
    v.y *= 0.01;
    m_ball->m_body->ApplyLinearImpulse(v, m_ball->m_body->GetPosition());
    
    Hit(b1);
    Hit(b2);
}

void Game::Hit(b2Body* v)
{
    if(ground == v)
    {
        m_gamestate = lost;
    } else
    {
        m_scores += m_bricks->Hit(v);
    }
}

void Game::DeleteBall(CTween* pTween)
{
    if (g_pGame->m_ball != 0)
    {
        g_pGame->m_gamestate = ready;
    }
}
