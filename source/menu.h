#if !defined(__MENU_H__)
#define __MENU_H__

#include "Iw2DSceneGraph.h"
#include <list>

#include "Box2D.h"
#include "scenes.h"
#include "IwNUI.h"

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

class Menu : public Scene
{
public:
    CLabel *m_play;
    CLabel *m_exit;

    Menu();
    ~Menu();

    void init(float x, float y);
    
    virtual void Update(float deltaTime = 0.0f, float alphaMul = 1.0f);
    void Render();
};

#endif
