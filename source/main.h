#if !defined(__MAIN_H__)
#define __MAIN_H__

#include "IwTween.h"

#include "resources.h"

#include "menu.h"

using namespace IwTween;

extern Game * g_pGame;

extern CTweenManager *g_pTweener;

extern Resources *g_pResources;

extern SceneManager *g_pSceneManager;

extern Menu *g_pMenu;

// graphics constans, come from Sprites or hardcoded if no sprites available

const int BRICK_H = 30;
const int BRICK_W = 60;

// what resolution images designed for?
const int GRAPHICS_WIDTH = 480;

// physics converter
const int PHYSICS_KOEF = 32;

// board position from bottomdown in percents 
const int BOARD_POSITION = 10;

const int FONT_HEIGHT = 15;
const int FONT_DESIGN_WIDTH = 320;

extern float G_SCALE;

float toPhysics(float x);

float fromPhysics(float x);

float fromGraphics(float x);

float toGraphics(float x);

#endif  // __MAIN_H__
