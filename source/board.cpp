#include "IwGx.h"
#include "IwTween.h"
#include "board.h"
#include "game.h"
#include "main.h"

using namespace IwTween;

Board::Board():m_body(0)
{
    m_W = BRICK_W * 2;
    m_H = BRICK_H;
    
    m_Color = CColor(0x77,0x77,0x77,0xff);
}

void Board::Render()
{
    CDrawable::Render();

    CDrawable::PreRender();

    Iw2DFillRect(CIwFVec2::g_Zero, CIwFVec2(m_W, m_H));
}

void Board::init(float x, float y)
{
    m_X = x;
    m_Y = y;
}

float Board::GetWidth()
{
    return m_W;
}

void Board::Update(b2World *w)
{
    if(m_body != 0){
        w->DestroyBody(m_body);
    }
    
    CIwFVec2 t = m_MatGlobal.TransformVec(CIwFVec2::g_Zero);
    
    float x = toGraphics(t.x);
    float y = toGraphics(t.y);

    b2BodyDef def;
    def.type = b2_staticBody;
    def.position.Set(toPhysics(x + m_W/2), toPhysics(y + m_H/2));
    m_body = w->CreateBody(&def);
    b2PolygonShape box;
    box.SetAsBox(toPhysics(m_W/2), toPhysics(m_H/2));
    b2FixtureDef fd;
    fd.shape = &box;
    fd.density = 0;
    fd.friction = 0;
    fd.restitution = 0;
    m_body->CreateFixture(&fd);
    m_body->SetUserData(this);
}
