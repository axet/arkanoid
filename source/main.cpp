#include "Iw2D.h"
#include "IwTween.h"
#include "input.h"
#include "game.h"
#include "main.h"
#include "IwGx.h"

using namespace IwTween;

// FRAME_TIME is the amount of time that a single frame should last in seconds
#define FRAME_TIME  (30.0f / 1000.0f)

// Global tweener is used by tweens that ned to be ranm outside of a scene
CTweenManager *g_pTweener = 0;

SceneManager *g_pSceneManager = 0;

Game *g_pGame = 0;

Resources *g_pResources = 0;

Menu *g_pMenu = 0;

float G_SCALE;

float toPhysics(float x)
{
    return x / PHYSICS_KOEF;
}

float fromPhysics(float x)
{
    return x * PHYSICS_KOEF;
}

float toGraphics(float x)
{
    return x / G_SCALE;
}

float fromGraphics(float x)
{
    return x * G_SCALE;
}

int main()
{
    // Initialise the 2D graphics system
    Iw2DInit();

    G_SCALE = IwGxGetScreenWidth() / (float)GRAPHICS_WIDTH;

    // Create global tween manager
    g_pTweener = new CTweenManager();
    
    g_pResources = new Resources();
    
    g_pSceneManager = new SceneManager();

    g_pInput = new Input();

    g_pMenu = new Menu();
    
    g_pGame =  new Game();
    
    g_pSceneManager->m_current = g_pMenu;

    // Loop forever, until the user or the OS performs some action to quit the app
    while (!s3eDeviceCheckQuitRequest())
    {
        uint64 new_time = s3eTimerGetMs();

        // Update input system
        g_pInput->Update();

        // Update global tween manager
        g_pTweener->Update(FRAME_TIME);

        // Update scene manager
        g_pSceneManager->Update(FRAME_TIME);

        // Clear the drawing surface
        Iw2DSurfaceClear(0xff000000);

        // Render scene manager
        g_pSceneManager->Render();

        // Show the drawing surface
        Iw2DSurfaceShow();

        // Lock frame rate
        int yield = (int)(FRAME_TIME * 1000 - (s3eTimerGetMs() - new_time));
        if (yield < 0)
            yield = 0;
        // Yield to OS
        s3eDeviceYield(yield);
    }

    delete g_pGame;
    delete g_pMenu;
    delete g_pResources;
    delete g_pInput;
    delete g_pTweener;
    delete g_pSceneManager;
    Iw2DTerminate();

    return 0;
}
