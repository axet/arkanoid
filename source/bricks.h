#if !defined(__BRICKS_H__)
#define __BRICKS_H__

#include "Iw2DSceneGraph.h"

#include <list>

#include "Box2D.h"
#include <IwTween.h>

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;
using namespace IwTween;

class Brick : public CDrawable
{
    CIwColour* m_type;
    CIwMaterial* m_material;
public:
    b2Body *m_body;
    bool m_delete;

    // how much score brick gives
    int score;

    Brick();
    ~Brick();
    
    void init(float x, float y, int type);
    
    void Render();

    void Update(b2World *w);
};

class Bricks : public CNode
{
protected:
    Brick** m_bricks;
    int m_width, m_height;

public:
    Bricks();
    ~Bricks();

    void NewGame();
    int Hit(b2Body*v);
    void Update(b2World *w);
};

#endif
