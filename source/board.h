#if !defined(__BOARD_H__)
#define __BOARD_H__

#include "Iw2DSceneGraph.h"
#include <list>

#include "Box2D.h"

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

class Board : public CDrawable
{
protected:
public:
    b2Body *m_body;
    
    Board();
    ~Board() {}
    
    void init(float x, float y);

    void Render();
    
    float GetWidth();
    
    void Update(b2World *w);
};

#endif
