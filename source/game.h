#if !defined(__GAME_H__)
#define __GAME_H__

#include "bricks.h"
#include "board.h"
#include "ball.h"
#include "IwTween.h"
#include "scenes.h"

#include "Box2D.h"

using namespace IwTween;

enum GameState
{
    paused = 0,
    ready = 1,
    playing = 2,
    lost = 3,
    restarting = 4
};

class Game : public Scene, public b2ContactListener
{
protected:
    Bricks *m_bricks;
    Board *m_board;
    Ball *m_ball;
    GameState m_gamestate;

    // how screen scales to physics
    float m_graphicsScale;
    
    // board fixed position
    float m_board_y;
    // touch area
    float m_board_y_area;

    // box2d physics
    b2World* wp;
    b2Body *ground;
    
    int m_scores;
    CLabel *m_scoreLabel;
    
    int m_lives;
    CLabel *m_liveLabel;
    
    virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
    void Hit(b2Body*);

public:
    Game();
    ~Game();
    
    void NewGame();

    void InitBall();
    void Update(float deltaTime = 0.0f, float alphaMul = 1.0f);
    void Render();
    b2Body* AddBorder(int x, int y, int xx, int yy);

    static void DeleteBall(CTween* pTween);
};

#endif  // __GAME_H__


